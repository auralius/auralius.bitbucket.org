d = zeros(1, 4);
a = [0.1 0.1 0.1 0.1]; % this is l1, l2, l3, and l4
alpha = zeros(1, 4);
offset = zeros(1, 4);

theta = [0 0 0 0]; % joint values

T = fkine_dh(4, theta, d, a, alpha, offset)