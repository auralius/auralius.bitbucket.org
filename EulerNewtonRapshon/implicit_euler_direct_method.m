% =========================================================================
% Solve M x_ddot + B x_dot + K x = F
% Implicit Euler with Newton Raphson
% =========================================================================

function implicit_euler_direct_method()
    clear all;
    close all;
    clc;
    
    figure;
    hold;
    
    K = 100;           % spring stiffness. N/m
    M = 5;             % mass, kg
    B = 2*sqrt(K*M);  %c ritical damping
    %B = 0;   
    
    % initial condition:
    x0 = 0.1;
    v0 = 0;
    F = 0;
    
    h = 1; % sampling rate
    t_start = 0;
    t_end = 10;
    t=t_start:h:t_end;
    
    % Use ode45, 1kHz as ground truth:    
    [tode45,xode45]=ode45(@msd, [t_start:0.001:t_end], [x0 v0], [], ...
                          M, B, K);
    plot(tode45,xode45(:,1), '--r')
    
    for k = 1 : length(t)
        
        v1 = (M / h * v0  + F - K * x0) / (M / h + B + K * h);
        x1 = x0 + v1 * h;   
        x0 = x1;
        v0 = v1;        
        
        x(k, :) = [x1 v1];
    end
    plot(t, x(:,1), 'b')   
    
    legend('ode45', 'Implicit Euler');
    xlabel('Time (s)')
    s = strcat('h = ', num2str(h), ' seconds');
    title(s);
end



function xdot=msd(t,x, M, B, K)
    xdot_1 = x(2);
    xdot_2 = -(B/M)*x(2) - (K/M)*x(1);

    xdot = [xdot_1 ; xdot_2 ];
end

