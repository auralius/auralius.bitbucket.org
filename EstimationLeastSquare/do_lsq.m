clc
clear all
close all
load dataset.mat

% First, calculate acceleration, we don't have this data
pos = response(:,1);
vel = response(:,2);
acc = zeros(N, 1);


for i = 2:length(response)
    acc(i) = (vel(i) - vel(i-1))  / ts;
end

A = [pos vel acc];
B = stimulus;

% AX = B, therefore:
X = A\B;

disp('Estimate:')
fprintf('k_est = %f\nb_est = %f\nm_est = %f\n', X(1), X(2), X(3));
disp('True:')
fprintf('k = %f\nb = %f\nm = %f\n', k, b, m);