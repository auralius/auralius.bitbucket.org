close all;
clear all;
clc;

Ts = 0.01;
tsim = 3;

K = 1000;
x_wall = 0.1;

m = 1;

F = 1;
pos_ = [];
for n = 0:0.1:0.5
    b = n*K*Ts;
    sim('virtual_wall.slx')
    pos_ = [pos_ pos];    
end
t_ = t;

figure
hold on
plot(t, pos_(:,1), '--')
plot(t, pos_(:,2))
plot(t, pos_(:,3))
plot(t, pos_(:,4))
plot(t, pos_(:,5))
plot(t, pos_(:,6))

legend('b=0', 'b=0.1*K*Ts', 'b=0.2*K*Ts', 'b=0.3*K*Ts', 'b=0.4*K*Ts', 'b=0.5*K*Ts' )