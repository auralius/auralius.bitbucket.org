function simulate()

clc
clear all
close all

m = 1;
b = 2;
k = 3;

ts = 1e-3;
tsim = 10;
time_span = 0:ts:tsim;
N = length(time_span);
 
initial_position = 0; 
initial_speed    = 0; 
 
x0 = [initial_position  initial_speed]; 

A = [0 1; -k/m -b/m];
B = [0; 1];
C = [1 0; 0 1];

sys = ss(A, B, C, 0);

stimulus = ones(N, 1) *0.1; %step
%stimulus = sin(2*pi*1*time_span)'; %sinusoid
response = lsim(sys, stimulus, time_span, x0);

save dataset.mat stimulus response time_span m b k ts N;