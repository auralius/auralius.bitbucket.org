figure;
hold on;

%% Our discretization
M = 1;
K = 1;
B = 1;

Tsim = 10;
T = 1e-3;
t = 0:ts:Tsim;

f = sin(2*pi*0.5*t);

x = zeros(length(t), 1);

for k = 3 : length(t)
    x(k) = (T^2*f(k) + (2*M+B*T)*x(k-1)-M*x(k-2)) / (M+T*B+K*T^2);
end

plot(t, x, 'b');


%% Compare with MATLAB, to make sure the derived soultion is fine 
H = tf(1, [M B K]);
x = lsim(H, f, t);
plot(t, x, '-.r', 'LineWidth', 1);

legend('Derived solution', 'MATLAB lsim')
xlabel('time')
ylabel('x')