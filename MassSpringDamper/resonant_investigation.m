clc;
clear all;
close all;

omega_n = 1;
zeta = 0.01:0.19:1;

figure;
hold on;
for k = 1 : length(zeta)
    sys = tf(1,[1 2*zeta(k)*omega_n omega_n^2])
    bode(sys)
    legends{k} = ['$\zeta=',num2str(zeta(k)), '$'];
end

h = legend(legends);
set(h,'Interpreter','latex')
xlim([0.1 10])

%%
omega_n = 1;
zeta = 0.01;
sys = tf(1,[1 2*zeta*omega_n omega_n^2])
figure;
bode(sys);


figure;
t = 0:0.001:1000;
u = sin(1/omega_n*t);
x = lsim(sys, u, t);
plot(t, x);
title('Input of 1 hz is applied for system with $\omega_n=1$', 'Interpreter','latex');

xlabel('$t$', 'Interpreter','latex')
ylabel('$x(t)$', 'Interpreter','latex')